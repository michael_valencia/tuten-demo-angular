import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BookingService {

  private _bookingUrl = `https://dev.tuten.cl/TutenREST/rest/user/contacto%40tuten.cl/bookings?current=true`;
  private httpOptions;

  constructor(private http: HttpClient) { }

  getBooking()  {

    this.httpOptions = {
      headers: new HttpHeaders({
        'Accept':  'application/json',
        'adminemail' : localStorage.getItem('email'),
        'token' : localStorage.getItem('token'),
        'app' : 'APP_BCK'
      })
    };

    return this.http.get<any>(this._bookingUrl, this.httpOptions);
  }
}
