import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {

  userLogin = {
    email : 'testapis@tuten.cl',
    password : '1234'
  };
  isDisableButton = false;
  textoButton = 'Iniciar sesión';

  constructor(private _auth: AuthService,
              private _router: Router) { }

  ngOnInit() {}

  loginUser()  {
    this.isDisableButton = true;
    this.textoButton  = 'Espere un momento';
    this._auth.loginUser(this.userLogin)
        .subscribe(
          res => {
              const array = res;
              localStorage.setItem(`token`, (array['sessionTokenBck'] + ''));
              localStorage.setItem(`email`, this.userLogin.email);
              this._router.navigate(['/booking']);
            },
          err => {
            this.isDisableButton = false;
            this.textoButton  = 'Iniciar sesión';
            alert('Opps, se ha presentado el siguiente error: ' + err.error);
          }
        );
  }
}
