import { Component, OnInit, SimpleChanges } from '@angular/core';
import { BookingService } from '../booking.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-booking',
  templateUrl: './booking.component.html',
  styleUrls: ['./booking.component.sass']
})
export class BookingComponent implements OnInit {

  bookings = [];
  bookingsFilterTable = [];
  filterBookingIdLike = '';
  filterBookingIdMajor = '';
  filterBookingIdMinor = '';

  filterPrecioIdLike = '';
  filterPrecioIdMajor = '';
  filterPrecioIdMinor = '';

  showTextLoading = true;


  constructor(private _booking: BookingService,
            private _router: Router) { }

  ngOnInit() {
    this._booking.getBooking()
      .subscribe(
        res => {
            this.bookings = <any>res;
            this.bookingsFilterTable = <any>res;
            this.showTextLoading = false;
          },
        err => {
          this._router.navigate(['/login']);
        }
      );
  }

  filterTable(e) {

          this.bookingsFilterTable = this.bookings.filter(booking => {
            if (  ( (this.filterBookingIdLike != '' && this.filterBookingIdLike != null) ? (booking.bookingId + '').includes(this.filterBookingIdLike) : true )  &&
                  ( (this.filterBookingIdMinor != '' && this.filterBookingIdMinor != null) ? (booking.bookingId <= this.filterBookingIdMinor) : true ) &&
                  ( (this.filterBookingIdMajor != '' && this.filterBookingIdMajor != null) ? (booking.bookingId >= this.filterBookingIdMajor) : true ) &&
                  ( (this.filterPrecioIdLike != '' && this.filterPrecioIdLike != null) ? (booking.bookingPrice + '').includes(this.filterPrecioIdLike) : true )  &&
                  ( (this.filterPrecioIdMinor != '' && this.filterPrecioIdMinor != null) ? (booking.bookingPrice <= this.filterPrecioIdMinor) : true ) &&
                  ( (this.filterPrecioIdMajor != '' && this.filterPrecioIdMajor != null) ? (booking.bookingPrice >= this.filterPrecioIdMajor) : true )
                ) return booking;
          });

  }

}
