import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private _loginUrl = `https://dev.tuten.cl/TutenREST/rest/user/testapis%40tuten.cl`;
  private httpOptions;

  constructor(private http: HttpClient,
            private _router: Router) { }

  loginUser(user)  {

    this.httpOptions = {
      headers: new HttpHeaders({
        'Accept':  'application/json',
        'password' : user.password,
        'app' : 'APP_BCK'
      })
    };

    return this.http.put<any>(this._loginUrl, {
          email : user.email
        }, this.httpOptions);
  }

  loggedInToken()   {
    return !!localStorage.getItem(`token`);
  }

  loggedInEmail()   {
    return !!localStorage.getItem(`email`);
  }

  logoutUser() {
    localStorage.removeItem(`token`);
    this._router.navigate(['\login']);
  }
}
