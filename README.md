## Url de acceso a la aplicación

http://tuten-demo-angular.adp-solutions.com

## Componentes utilizados - TutenDemoWebApp

Proyecto generado con [Angular CLI](https://github.com/angular/angular-cli) version 7.2.2.
Bootstrap v4.2.1
Node v10.13

## Servidor de desarrollo

Ejecutar `ng serve` en la consola de comandos. Navegar a `http://localhost:4200/`. 

## Estructura del proyecto

Creación de componentes login y booking
Utilización del Routing de Angular
Protección de ruta booking solo cuando inicia sesión
Utilización de localStorage para almacenar el Token

## Ejecutar proyecto

Clonar de: git clone https://michael_valencia@bitbucket.org/michael_valencia/tuten-demo-angular.git
Ejecutar: npm install
Ejecutar `ng serve` en la consola de comandos. Navegar a `http://localhost:4200/`. 
